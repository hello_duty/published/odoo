# -*- coding: utf-8 -*-
{
    'name': "Helloduty",

    'summary': """
        Make and receive calls and SMS, sync contacts, 
        capture conversation history and recordings right from your Odoo Platform. 
        EE (Enterprise Edition) and CE (Community Edition)
    """,

    'description': """
        For Odoo 16 EE + CE
    """,

    'author': "Helloduty",
    'website': "https://www.helloduty.com/",

    'category': 'Productivity',
    'version': '1.0',
    'license': 'OPL-1',
    'application': True,
    'installable': True,
    'auto_install': False,


    # any module necessary for this one to work correctly
    'depends': ['base', 'web'],
    'data': [
           'views/settings.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'helloduty/static/src/js/app.js'
        ],
    },
    "images": ["static/src/img/phone.svg", "static/src/img/close.svg"],
}
